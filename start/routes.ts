/*
|--------------------------------------------------------------------------
| Routes file
|--------------------------------------------------------------------------
|
| The routes file is used for defining the HTTP routes.
|
*/

import router from '@adonisjs/core/services/router'
import { middleware } from '#start/kernel'

const UploadsController = () => import('#controllers/uploads_controller')
const AuthController = () => import('#controllers/auth_controller')
const UsersController = () => import('#controllers/users_controller')
const PostsController = () => import('#controllers/posts_controller')

router.get('uploads/profile-pictures/*', [UploadsController, 'serveProfilePicture'])

router.post('auth/login', [AuthController, 'login'])
router.post('auth/logout', [AuthController, 'logout']).use(middleware.auth())
router.put('auth/me', [AuthController, 'update']).use(middleware.auth())
router
    .put('auth/me/profile-picture', [AuthController, 'updateProfilePicture'])
    .use(middleware.auth())
router.delete('auth/me', [AuthController, 'destroy']).use(middleware.auth())

router.get('users', [UsersController, 'index'])
router.get('users/:id', [UsersController, 'show']).where('id', router.matchers.number())
router.post('users', [UsersController, 'store'])
router.get('users/:id/posts', [PostsController, 'fromUser']).where('id', router.matchers.number())

router.post('posts', [PostsController, 'store']).use(middleware.auth())
router
    .put('posts/:id', [PostsController, 'update'])
    .where('id', router.matchers.number())
    .use(middleware.auth())
router
    .delete('posts/:id', [PostsController, 'destroy'])
    .where('id', router.matchers.number())
    .use(middleware.auth())
