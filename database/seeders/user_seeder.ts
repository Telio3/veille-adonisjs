import { BaseSeeder } from '@adonisjs/lucid/seeders'
import { UserFactory } from '#database/factories/user_factory'

export default class extends BaseSeeder {
    async run() {
        await UserFactory.with('posts', 3).createMany(10)
    }
}
