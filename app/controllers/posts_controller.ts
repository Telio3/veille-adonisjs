import Post from '#models/post'
import PostService from '#services/post_service'
import { createPostValidator, updatePostValidator } from '#validators/post'
import { inject } from '@adonisjs/core'
import type { HttpContext } from '@adonisjs/core/http'

@inject()
export default class PostsController {
    constructor(private readonly postService: PostService) {}

    async fromUser({ params }: HttpContext): Promise<Post[]> {
        return this.postService.allByUser(params.id)
    }

    async store({ auth, request }: HttpContext): Promise<Post> {
        const user = auth.user!

        const payload = await request.validateUsing(createPostValidator)

        return this.postService.create(payload, user)
    }

    async update({ auth, params, request }: HttpContext): Promise<Post> {
        const user = auth.user!

        const payload = await request.validateUsing(updatePostValidator)

        return this.postService.update(params.id, payload, user)
    }

    async destroy({ auth, params, response }: HttpContext): Promise<void> {
        const user = auth.user!

        await this.postService.delete(params.id, user)

        response.noContent()
    }
}
