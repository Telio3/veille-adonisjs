import type { HttpContext } from '@adonisjs/core/http'
import { sep, normalize } from 'node:path'
import app from '@adonisjs/core/services/app'
import BadRequestException from '#exceptions/bad_request_exception'

const PATH_TRAVERSAL_REGEX = /(?:^|[\\/])\.\.(?:[\\/]|$)/

export default class UploadsController {
    serveProfilePicture({ request, response }: HttpContext): void {
        const filePath = request.param('*').join(sep)
        const normalizedPath = normalize(filePath)

        if (PATH_TRAVERSAL_REGEX.test(normalizedPath)) {
            throw new BadRequestException('Malformed path')
        }

        const absolutePath = app.makePath('storage', 'uploads', 'profile_pictures', normalizedPath)

        return response.download(absolutePath)
    }
}
