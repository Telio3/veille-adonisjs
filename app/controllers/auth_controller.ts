import type { HttpContext } from '@adonisjs/core/http'
import { inject } from '@adonisjs/core'
import AuthService from '#services/auth_service'
import User from '#models/user'
import { updateUserValidator } from '#validators/user'
import { loginValidator } from '#validators/auth'
import BadRequestException from '#exceptions/bad_request_exception'

@inject()
export default class AuthController {
    constructor(private readonly authService: AuthService) {}

    async login({ request }: HttpContext): Promise<{ user: User; token: string }> {
        const payload = await request.validateUsing(loginValidator)

        const { user, token } = await this.authService.login(payload)

        return {
            user,
            token,
        }
    }

    async logout({ auth, response }: HttpContext): Promise<void> {
        const user = auth.user!

        await this.authService.logout(user, user.currentAccessToken!)

        response.noContent()
    }

    async update({ auth, request }: HttpContext): Promise<User> {
        const payload = await request.validateUsing(updateUserValidator)

        return this.authService.update(auth.user!, payload)
    }

    async destroy({ auth, response }: HttpContext): Promise<void> {
        await this.authService.delete(auth.user!)

        response.noContent()
    }

    async updateProfilePicture({ auth, request }: HttpContext): Promise<User> {
        const profilePicture = request.file('profile_picture', {
            size: '2mb',
            extnames: ['jpg', 'jpeg', 'png'],
        })

        if (!profilePicture) throw new BadRequestException('Profile picture is required')

        if (!profilePicture.isValid) throw new BadRequestException('Invalid profile picture')

        return this.authService.updateProfilePicture(auth.user!, profilePicture)
    }
}
