import type { HttpContext } from '@adonisjs/core/http'
import { inject } from '@adonisjs/core'
import UserService from '#services/user_service'
import User from '#models/user'
import { createUserValidator } from '#validators/user'

@inject()
export default class UsersController {
    constructor(private readonly userService: UserService) {}

    index(): Promise<User[]> {
        return this.userService.all()
    }

    show({ params }: HttpContext): Promise<User | null> {
        return this.userService.find(params.id)
    }

    async store({ request }: HttpContext): Promise<User> {
        const payload = await request.validateUsing(createUserValidator)

        return this.userService.create(payload)
    }
}
