import Post from '#models/post'
import { Infer } from '@vinejs/vine/types'
import { createPostValidator, updatePostValidator } from '#validators/post'
import User from '#models/user'
import UnAuthorizedException from '#exceptions/un_authorized_exception'

type CreatePostPayload = Infer<typeof createPostValidator>
type UpdatePostPayload = Infer<typeof updatePostValidator>

export default class PostService {
    async allByUser(userId: number): Promise<Post[]> {
        const posts = await Post.query()
            .where('user_id', userId)
            .orderBy('created_at', 'desc')
            .exec()

        return posts
    }

    async create(data: CreatePostPayload, user: User): Promise<Post> {
        const post = new Post()

        post.merge(data)
        await post.related('user').associate(user)

        await post.save()

        return post
    }

    async update(id: number, data: UpdatePostPayload, user: User): Promise<Post> {
        const post = await Post.findOrFail(id)

        if (post.userId !== user.id) {
            throw new UnAuthorizedException('You are not allowed to update this post')
        }

        post.merge(data)

        await post.save()

        return post
    }

    async delete(id: number, user: User): Promise<void> {
        const post = await Post.findOrFail(id)

        if (post.userId !== user.id) {
            throw new UnAuthorizedException('You are not allowed to delete this post')
        }

        await post.delete()
    }
}
