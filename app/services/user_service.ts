import User from '#models/user'
import { Infer } from '@vinejs/vine/types'
import { createUserValidator } from '#validators/user'

type CreateUserPayload = Infer<typeof createUserValidator>

export default class UserService {
    async all(): Promise<User[]> {
        const users = await User.query().orderBy('full_name', 'asc').exec()

        return users
    }

    async find(id: number): Promise<User | null> {
        const user = await User.findOrFail(id)

        return user
    }

    async create(data: CreateUserPayload): Promise<User> {
        const user = await User.create(data)

        return user
    }
}
