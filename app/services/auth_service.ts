import User from '#models/user'
import { loginValidator } from '#validators/auth'
import { updateUserValidator } from '#validators/user'
import { AccessToken } from '@adonisjs/auth/access_tokens'
import { MultipartFile } from '@adonisjs/core/bodyparser'
import { cuid } from '@adonisjs/core/helpers'
import app from '@adonisjs/core/services/app'
import { Infer } from '@vinejs/vine/types'

type UpdateUserPayload = Infer<typeof updateUserValidator>
type LoginPayload = Infer<typeof loginValidator>

export default class AuthService {
    async login({ email, password }: LoginPayload): Promise<{ user: User; token: string }> {
        const user = await User.verifyCredentials(email, password)

        if (!user) throw new Error('Invalid credentials')

        const token = await User.accessTokens.create(user)

        return {
            user,
            token: token.value!.release(),
        }
    }

    async logout(user: User, token: AccessToken): Promise<void> {
        await User.accessTokens.delete(user, token.identifier)
    }

    async update(user: User, data: UpdateUserPayload): Promise<User> {
        user.merge(data)

        await user.save()

        return user
    }

    async delete(user: User): Promise<void> {
        await user.delete()
    }

    async updateProfilePicture(user: User, profilePicture: MultipartFile): Promise<User> {
        await profilePicture.move(app.makePath('storage/uploads/profile_pictures'), {
            name: `${cuid()}.${profilePicture.extname}`,
        })

        user.profilePicture = profilePicture.fileName!

        await user.save()

        return user
    }
}
