import vine from '@vinejs/vine'

export const createUserValidator = vine.compile(
    vine.object({
        fullName: vine.string().trim(),
        email: vine
            .string()
            .trim()
            .toLowerCase()
            .email()
            .unique(async (db, value) => {
                const user = await db.from('users').where('email', value).first()
                return !user
            }),
        password: vine
            .string()
            .minLength(8)
            .maxLength(32)
            .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/),
    })
)

export const updateUserValidator = vine.compile(
    vine.object({
        fullName: vine.string(),
    })
)
